﻿using System;
using System.Linq;

namespace SoaLab1
{
    public static class InputToOutput
    {
        public static Output Convert(Input Input)
        {
            Output Output = new Output();
            Output.SumResult = Input.Sums.Sum() * Input.K;
            Output.MulResult = Input.Muls.Aggregate((agg, i) => agg * i);
            Output.SortedInputs = Input.Sums.Concat(Array.ConvertAll(Input.Muls, x => (decimal)x)).ToArray();
            Array.Sort(Output.SortedInputs);
            return Output;
        }
    }
}
