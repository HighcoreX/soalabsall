﻿using System;

namespace SoaLab1
{
    class Program
    {
        static void Main(string[] args)
        {
            String SerializeType = Console.ReadLine();
            String SerializedObject = Console.ReadLine();
            if (String.IsNullOrEmpty(SerializeType) || String.IsNullOrEmpty(SerializedObject)) { return; }
            Output Output;
            Input Input;
            ISerializer Serializer;
            switch (SerializeType)
            {
                case "Xml":
                    Serializer = new TheXmlSerializer();
                    break;
                case "Json":
                    Serializer = new TheJsonSerializer();
                    break;
                default:
                    return;
            }
            Input = Serializer.Deserialize<Input>(SerializedObject);
            Output = InputToOutput.Convert(Input);
            Console.Write(Serializer.Serialize<Output>(Output));
        }
    }
}