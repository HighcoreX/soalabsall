﻿using System.Text;
using System.Xml;
using System.Xml.Serialization;
using System.Text.RegularExpressions;
using System.IO;

namespace SoaLab1
{
    public class TheXmlSerializer : ISerializer
    {
        public string Serialize<T>(T Data)
        {
            XmlSerializer Serializer = new XmlSerializer(typeof(T));
            XmlSerializerNamespaces XmlNamespaces = new XmlSerializerNamespaces(new[] { XmlQualifiedName.Empty });
            XmlWriterSettings XmlSettings = new XmlWriterSettings
            {
                Indent = true,
                OmitXmlDeclaration = true,
                Encoding = new UTF8Encoding(false)
            };
            StringBuilder XmlString = new StringBuilder();
            XmlWriter XmlWriterEx = XmlWriter.Create(XmlString, XmlSettings);
            Serializer.Serialize(XmlWriterEx, Data, XmlNamespaces);
            return Regex.Replace(XmlString.ToString(), @"[\r\n\s]", "", RegexOptions.None);
        }
        public T Deserialize<T>(string Data)
        {
            XmlSerializer Serializer = new XmlSerializer(typeof(T));
            TextReader XmlReader = new StringReader(Data);
            return (T)Serializer.Deserialize(XmlReader);
        }
    }
}
