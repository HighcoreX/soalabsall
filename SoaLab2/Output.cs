﻿using Newtonsoft.Json;

namespace SoaLab2
{
    [JsonObject(MemberSerialization.OptIn)]
    public class Output
    {
        [JsonProperty]
        public decimal SumResult { get; set; }
        [JsonProperty]
        public int MulResult { get; set; }
        [JsonProperty]
        public decimal[] SortedInputs { get; set; }
    }
}
