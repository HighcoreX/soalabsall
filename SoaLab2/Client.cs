﻿using System;
using System.Net.Http;

namespace SoaLab2
{
    public class Client : IClient
    {
        private string URL { get; set; }
        public Client(string port)
        {
            URL = String.Format("http://127.0.0.1:{0}", port);
        }
        public HttpResponseMessage Get(string RequestMethod)
        {
            using (HttpClient HttpClient = new HttpClient())
            {
                HttpClient.BaseAddress = new Uri(URL);
                return HttpClient.GetAsync(RequestMethod).Result;
            }
        }
        public HttpResponseMessage Post<T>(string RequestMethod, T Content)
        {
            using (HttpClient HttpClient = new HttpClient()) {
                HttpClient.BaseAddress = new Uri(URL);
                HttpContent SentHttpContent = new ByteArrayContent(BitConverter.GetBytes((dynamic)Content));
                HttpResponseMessage Response = HttpClient.PostAsync(RequestMethod, SentHttpContent).Result;
                HttpContent GivenHttpContent = Response.Content;
                return Response;
            }
        }

        //public bool Ping()
        //{
        //    using (var HttpClient = new HttpClient())
        //    {
        //        var Method = "/Ping";
        //        HttpClient.BaseAddress = new Uri(URL);
        //        var Response = HttpClient.GetAsync(Method);
        //        return Response.Result.IsSuccessStatusCode;
        //    }
        //}
        ////todo: работу с хттп и сериализацией нужно убрпть на другой уровень асбтракций
        //public string GetInputData()
        //{
        //    using (var HttpClient = new HttpClient())
        //    {
        //        var Method = "/GetInputData";
        //        HttpClient.BaseAddress = new Uri(URL);
        //        var Response = HttpClient.GetStringAsync(Method);
        //        return Response.Result;
        //    }
        //}
        //public void WriteAnswer(string Data)
        //{
        //    using (var HttpClient = new HttpClient())
        //    {
        //        var Method = "/WriteAnswer";
        //        HttpClient.BaseAddress = new Uri(URL);
        //        HttpContent Answer = new StringContent(Data);
        //        var Response = HttpClient.PostAsync(Method, Answer);
        //        HttpContent HttpContent = Response.Result.Content;
        //        string ResponseFromServer = HttpContent.ReadAsStringAsync().Result;
        //    }
        //}
    }
}
