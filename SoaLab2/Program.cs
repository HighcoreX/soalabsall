﻿using System;

namespace SoaLab2
{
    class Program
    {
        static void Main(string[] args)
        {
            string Port = Console.ReadLine();
            IClient BacsClient = new Client(Port);
            if (BacsClient.Get("/Ping").IsSuccessStatusCode)
            {
                ISerializer Serializer = new TheJsonSerializer();
                string InputString = BacsClient.Get("/GetInputData").Content.ReadAsStringAsync().Result;
                Input Input = Serializer.Deserialize<Input>(InputString);
                Output Output = InputToOutput.Convert(Input);
                string OuputString = Serializer.Serialize(Output);
                BacsClient.Post<String>("/WriteAnswer", OuputString);
            }
        }
    }
}
