﻿using System.Net.Http;

namespace SoaLab2
{
    interface IClient
    {
        HttpResponseMessage Get(string RequestMethod);
        HttpResponseMessage Post<T>(string RequestMethod, T Content);
    }
}
