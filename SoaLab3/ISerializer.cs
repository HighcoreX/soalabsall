﻿namespace SoaLab3
{
    interface ISerializer
    {
        string Serialize<T>(T Data);
        T Deserialize<T>(string Data);
    }
}
