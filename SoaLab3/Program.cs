﻿using System;

namespace SoaLab3
{
    class Program
    {
        static void Main(string[] args)
        {
            int Port = int.Parse(Console.ReadLine());
            Server Server = new Server(Port);
            Server.Start();
        }
    }
}
