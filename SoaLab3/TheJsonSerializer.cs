﻿using Newtonsoft.Json;

namespace SoaLab3
{
    public class TheJsonSerializer : ISerializer
    {
        public string Serialize<T>(T Data)
        {
            return JsonConvert.SerializeObject(Data);
        }
        public T Deserialize<T>(string Data)
        {
            return JsonConvert.DeserializeObject<T>(Data);
        }
    }
}