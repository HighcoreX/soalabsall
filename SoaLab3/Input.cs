﻿using Newtonsoft.Json;

namespace SoaLab3
{
    [JsonObject(MemberSerialization.OptIn)]
    public class Input
    {
        [JsonProperty]
        public int K { get; set; }
        [JsonProperty]
        public decimal[] Sums { get; set; }
        [JsonProperty]
        public int[] Muls { get; set; }
    }
}
