﻿using System;
using System.Net.Sockets;
using System.Net;

namespace SoaLab3
{
    public class Server
    {
        TcpListener Listener;
        ClientHandler ClientHandler = new ClientHandler();
        public Server(int Port)
        {
            Listener = new TcpListener(IPAddress.Any, Port);
        }
        public void Start() {
            Listener.Start();
            do { ClientThread(Listener.AcceptTcpClient()); } while (ClientHandler.Power);
        }
        private void ClientThread(Object StateInfo)
        {
            ClientHandler.Start((TcpClient)StateInfo);
            if (!ClientHandler.Power) Listener.Stop();
        }
        ~Server()
        {
            if (Listener != null)
            {
                Listener.Stop();
            }
        }
    }
}
